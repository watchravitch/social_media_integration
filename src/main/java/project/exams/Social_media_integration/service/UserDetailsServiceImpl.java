package project.exams.Social_media_integration.service;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import project.exams.Social_media_integration.common.AuthProvider;
import project.exams.Social_media_integration.dao.UserRepository;
import project.exams.Social_media_integration.entity.RoleDetailsDTO;
import project.exams.Social_media_integration.entity.UserDetailsDTO;
import project.exams.Social_media_integration.model.RegisterUser;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {


	@Autowired
	UserRepository userRepository;
	
	@Autowired
	PasswordEncoder encode;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		System.out.println("fetching user details");
		UserDetailsDTO userDetails = userRepository.findByUsernameamdprovider(username,"local");
		System.out.println(userDetails);
		if(userDetails == null) {
			System.out.println("Throwing...");
//			throw new RuntimeException("check it");
			throw new RuntimeException("Username:"+username +" not found!");
		} 
		
		if(userDetails != null && !AuthProvider.local.equals(userDetails.getProvider())) {
			System.out.println("Throwing user provider issue");
			throw new RuntimeException("Username:"+username +" has logged in from "+userDetails.getProvider()+". Please login with this provider");
		}
		return userDetails;
	}
	
	
	public void registerUser(RegisterUser registerUser, AuthProvider provider) {


		UserDetailsDTO userDetailsDTO = new UserDetailsDTO();
		if(!isNullOrEmpty(registerUser.getEmail_id()))
		userDetailsDTO.setEmailId(registerUser.getEmail_id());
		userDetailsDTO.setUsername(registerUser.getEmail());
		userDetailsDTO.setPassword(encode.encode(registerUser.getPassword()));
		userDetailsDTO.setFirstName(registerUser.getFirstName());
		userDetailsDTO.setLastName(registerUser.getLastName());
		RoleDetailsDTO roleDetails = new RoleDetailsDTO();
		roleDetails.setRoleId("ROLE_USER");
		HashSet<RoleDetailsDTO> roleSet = new HashSet<>();
		userDetailsDTO.setAuthorities(roleSet);
		//When user registers through application. Provider would be Local
		userDetailsDTO.setProvider(provider);
		
		userRepository.save(userDetailsDTO);
	}
	public static boolean isNullOrEmpty(String str) {
		if(str != null && !str.trim().isEmpty())
			return false;
		return true;
	}

}
