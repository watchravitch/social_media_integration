package project.exams.Social_media_integration.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;
import project.exams.Social_media_integration.common.OAuth2UserFactory;
import project.exams.Social_media_integration.dao.UserRepository;
import project.exams.Social_media_integration.entity.UserDetailsDTO;
import project.exams.Social_media_integration.model.OAuth2UserInfo;
import project.exams.Social_media_integration.model.RegisterUser;


@Service
public class OAuth2ServiceImpl extends DefaultOAuth2UserService {

	@Autowired
	UserRepository userRepo;
	
	@Autowired 
	UserDetailsServiceImpl userService;
	
	@Override
	public OAuth2User loadUser(OAuth2UserRequest userRequest) throws OAuth2AuthenticationException {
	 	 
		// instead of calling to get basic info 
		  //OAuth2User oauth2User = super.loadUser(userRequest);
	 	
	 	// call here to get complete information
	 	OAuth2UserInfo oauth2UserInfo = OAuth2UserFactory.getOAuth2User(userRequest);
	    return 	processUser(oauth2UserInfo);
	}
	
	private UserDetailsDTO processUser(OAuth2UserInfo userInfo) {
		UserDetailsDTO  userDetailsDTO =  userRepo.findByUsernameamdprovider(userInfo.getEmail(), String.valueOf(userInfo.getProvider()));
		if(userDetailsDTO != null) {
			// checking if user has logged in with different provider
			userDetailsDTO.setUserId(userDetailsDTO.getUserId());
			userDetailsDTO.setFirstName(userInfo.getFirstName());
			userDetailsDTO.setLastName(userInfo.getLastName());
			userRepo.save(userDetailsDTO);
		}  else {
			RegisterUser registerUser = new RegisterUser();
			registerUser.setEmail_id(userInfo.getId());
			registerUser.setEmail(userInfo.getEmail());
			registerUser.setFirstName(userInfo.getFirstName());
			registerUser.setLastName(userInfo.getLastName());
			registerUser.setPassword(userInfo.getId()+userInfo.getFirstName());
			userService.registerUser(registerUser, userInfo.getProvider());
			userDetailsDTO =  userRepo.findByEmail(userInfo.getId());
		}
		return userDetailsDTO;
	}

}
