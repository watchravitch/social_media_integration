package project.exams.Social_media_integration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class SocialMediaIntegrationApplication {
	public static void main(String[] args) {
		SpringApplication.run(SocialMediaIntegrationApplication.class, args);
	}

}
