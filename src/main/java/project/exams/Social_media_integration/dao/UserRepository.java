package project.exams.Social_media_integration.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import project.exams.Social_media_integration.entity.UserDetailsDTO;


@Repository
public interface UserRepository extends JpaRepository<UserDetailsDTO, String>{

	@Query("select u from UserDetailsDTO u where username=?1")
	public UserDetailsDTO findByUsername(String username);
	
	@Query("select u from UserDetailsDTO u where emailId=?1")
	public UserDetailsDTO findByEmail(String emailId);

	@Query(value = "SELECT * FROM user_details WHERE user_name=?1 AND provider=?2",nativeQuery = true)
	public UserDetailsDTO findByUsernameamdprovider(String username,String provider);
	
}
