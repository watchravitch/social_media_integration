package project.exams.Social_media_integration.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class RegisterUser {
	private String email_id;
	private String email;
	private String password;
	private String repassword;
	private String firstName;
	private String lastName;
	
}
