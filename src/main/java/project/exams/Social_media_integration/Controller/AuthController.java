package project.exams.Social_media_integration.Controller;

import com.sun.javafx.collections.MappingChange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import project.exams.Social_media_integration.common.AuthProvider;
import project.exams.Social_media_integration.dao.UserRepository;
import project.exams.Social_media_integration.entity.UserDetailsDTO;
import project.exams.Social_media_integration.model.RegisterUser;
import project.exams.Social_media_integration.service.UserDetailsServiceImpl;

import java.security.Principal;
import java.util.Collection;


@Controller
public class AuthController {

	@Autowired
	OAuth2AuthorizedClientService authclientService;

	@Autowired
	UserDetailsServiceImpl userDetailsService;

	@Autowired
	UserRepository userRepository;

	@GetMapping("/")
	public String home(Model model, @AuthenticationPrincipal UserDetailsDTO userDetails) {

		model.addAttribute("name", userDetails.getFirstName());
		return "home";
	}

	//@GetMapping("/formLoginSuccess") @AuthenticationPrincipal UserDetailsDTO userDetails
	@RequestMapping(value = "/formLoginSuccess", method = RequestMethod.GET)
	public String formSuccessLogin(Model model,Principal principal) {
		UserDetailsDTO user = userRepository.findByUsernameamdprovider(
				principal.getName(),"local");
		model.addAttribute("user",user);
		System.out.println(user.getProvider());
		return "home";
	}



	@RequestMapping(value = "/oauth2LoginSuccess", method = RequestMethod.GET)
	public String oauth2SuccessLogin(Model model, Principal principal) {

		UserDetailsDTO user = userRepository.findByUsernameamdprovider(
				principal.getName(),"facebook");
		model.addAttribute("user",user);
		System.out.println(user.getProvider());
		return "home";
	}

	@RequestMapping(value = "/logoutSuccessful", method = RequestMethod.GET)
	public String logoutSuccessfulPage(Model model) {
		model.addAttribute("title", "Logout");
		return "logoutSuccessfulPage";
	}

	// Returning the login page.
	@GetMapping("/login")
	public String login() {
		return "login";
	}

	@GetMapping("/register")
	public String register(@ModelAttribute("RegisterUser") RegisterUser RegisterUser) {
		return "register";
	}


	@PostMapping("/register")
	public String registerSuccess(@ModelAttribute("RegisterUser") RegisterUser RegisterUser) {
			register(RegisterUser, AuthProvider.local);
		return "login";
	}

	private void register(RegisterUser user, AuthProvider provider) {
		userDetailsService.registerUser(user,provider);
	}

}
