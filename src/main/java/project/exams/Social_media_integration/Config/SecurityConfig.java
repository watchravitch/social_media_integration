package project.exams.Social_media_integration.Config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import project.exams.Social_media_integration.service.OAuth2ServiceImpl;
import project.exams.Social_media_integration.service.UserDetailsServiceImpl;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
//.antMatchers("/oauth2/**").permitAll()
    @Autowired
    OAuth2ServiceImpl oauth2UserServiceImpl;

    @Autowired
    UserDetailsServiceImpl userDetailsService;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/login","/logout","/register").permitAll()
                .anyRequest().authenticated()
                .and().formLogin() // enable form based login
                .loginPage("/login").usernameParameter("username").passwordParameter("password").defaultSuccessUrl("/formLoginSuccess",true).failureUrl("/login?error=true")
                .and()
                .logout().logoutUrl("/logout").logoutSuccessUrl("/").deleteCookies("auth_code", "JSESSIONID").invalidateHttpSession(true) // enable logout
                .and().oauth2Login() // enable OAuth2
                .loginPage("/login").userInfoEndpoint().userService(oauth2UserServiceImpl).and().defaultSuccessUrl("/oauth2LoginSuccess",true)
                .and().rememberMe().key("uniqueKey").rememberMeParameter("rm")
                .and().csrf().disable(); // disable CSRF
    }
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        super.configure(auth);
        auth.userDetailsService(userDetailsService);
    }
    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
