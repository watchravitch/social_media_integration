 $('#sidenav-main').html("<div class='scrollbar-inner'>"
                         +"<!-- Brand -->"
                                  +"<div class='sidenav-header  align-items-center'>"
                                      +"<a class='navbar-brand' href=''>"
                                          +"<img src='assets/img/brand/tdid_logo2.png' class='navbar-brand-img'>"
                                      +"</a>"
                                  +"</div>"
                                  +"<div class='navbar-inner'>"
                                     +" <!-- Collapse -->"
                                     +" <div class='collapse navbar-collapse' id='sidenav-collapse-main'>"
                                         +" <!-- Nav items -->"
                                          +"<ul class='navbar-nav'>"
                                              +"<li class='nav-item'>"
                                                  +"<a class='nav-link' href='/login'><i class='fa fa-columns text-primary'></i><span class='nav-link-text'>Dashboard</span></a>"
                                              +"</li>"
                                              +"<li class='nav-item'>"
                                                  +"<a class='nav-link' href='/AuthCode'><i class='fa fa-cogs text-yellow'></i><span class='nav-link-text'>Authorization Code</span></a>"
                                              +"</li>"
                                             +"<li class='nav-item dropdown'>"
                                                  +"<a class='nav-link dropdown-toggle' data-toggle='dropdown'><i class='fa fa-certificate text-orange'></i><span class='nav-link-text'>Certificate Management</span></a>"
                                                  +"<div class='dropdown-menu dropdown-menu'>"
                                                      +"<a class='dropdown-item' href='#'><i class='ni ni-bold-right text-orange'></i>Revoke Certificate</a>"
                                                      +"<a class='dropdown-item' href='#'><i class='ni ni-bold-right text-orange'></i>Suspension Certificate</a>"
                                                      +"<a class='dropdown-item' href='#'><i class='ni ni-bold-right text-orange'></i>Unsuspension Certificate</a>"
                                                  +"</div>"
                                              +"</li>"
                                              +"<li class='nav-item dropdown'>"
                                                  +"<a class='nav-link' href='/CheckCert'><i class='fa fa-eye text-green'></i><span class='nav-link-text'>Check Status Certificate</span></a>"
                                              +"</li>"
                                              +"<li class='nav-item dropdown'>"
                                                    +"<a class='nav-link' href='/Tracking'><i class='fa fa-tag text-red'></i><span class='nav-link-text'>Tracking Application ID</span></a>"
                                              +"</li>"
                                              +"<li class='nav-item dropdown'>"
                                                  +"<a class='nav-link' href='/Download'><i class='fa fa-download text-default'></i><span class='nav-link-text'>Download Certificate</span></a>"
                                              +"</li>"
                                              +"<li class='nav-item dropdown'>"
                                                 +"<a class='nav-link' href='/Search'><i class='fa fa-search text-primary'></i><span class='nav-link-text'>Search Document</span></a>"
                                              +"</li>"
                                              +"<li class='nav-item'>"
                                                 +"<a class='nav-link' href='/Report'><i class='fa fa-book text-brown'></i><span class='nav-link-text'>Report</span></a>"
                                              +"</li>"
                                          +"</ul>"
                                          +"<!-- Divider -->"
                                          +"<hr class='my-3'>"
                                          +"<!-- Heading -->"
                                          +"<h6 class='navbar-heading p-0 text-muted'>"
                                              +"<span class='docs-normal'>Documentation</span>"
                                          +"</h6>"
                                          +"<!-- Navigation -->"
                                          +"<ul class='navbar-nav mb-md-3'>"
                                              +"<li class='nav-item'>"
                                                  +"<a class='nav-link' href='https://demos.creative-tim.com/argon-dashboard/docs/getting-started/overview.html' target='_blank'>"
                                                      +"<i class='ni ni-spaceship'></i>"
                                                      +"<span class='nav-link-text'>Manual Documentation</span>"
                                                  +"</a>"
                                              +"</li>"
                                          +"</ul>"
                                          +"<!-- Divider -->"
                                          +"<hr class='my-3'>"
                                          +"<!-- Heading -->"
                                          +"<h6 class='navbar-heading p-0 text-muted'>"
                                              +"<span class='docs-normal'>Web Site</span>"
                                          +"</h6>"
                                          +"<!-- Navigation -->"
                                          +"<ul class='navbar-nav mb-md-3'>"
                                              +"<li class='nav-item'>"
                                                  +"<a class='nav-link' href='https://www.thaidigitalid.com' target='_blank'>"
                                                      +"<i class='fa fa-home'></i>"
                                                      +"<span class='nav-link-text'>Home Page</span>"
                                                  +"</a>"
                                              +"</li>"
                                          +"</ul>"
                                     +"</div>"
                                  +"</div>"
                              +"</div>");

$('#footer').html("<div class='row align-items-center justify-content-lg-between'>"
+"                     <div class='col-lg-6'>"
+"                       <div class='copyright text-center  text-lg-left  text-muted'>"
+"                         &copy; 2020 <a href='https://www.thaidigitalid.com' class='font-weight-bold ml-1' target='_blank'>Thai Digital ID Company Limited</a>"
+"                       </div>"
+"                     </div>"
+"                     <div class='col-lg-6'>"
+"                       <ul class='nav nav-footer justify-content-center justify-content-lg-end'>"
+"                         <li class='nav-item'>"
+"                           <a href='https://www.creative-tim.com' class='nav-link' target='_blank'>Creative Tim</a>"
+"                         </li>"
+"                         <li class='nav-item'>"
+"                           <a href='https://www.creative-tim.com/presentation' class='nav-link' target='_blank'>About Us</a>"
+"                         </li>"
+"                         <li class='nav-item'>"
+"                           <a href='http://blog.creative-tim.com' class='nav-link' target='_blank'>Blog</a>"
+"                         </li>"
+"                         <li class='nav-item'>"
+"                           <a href='https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md' class='nav-link' target='_blank'>MIT License</a>"
+"                         </li>"
+"                       </ul>"
+"                     </div>"
+"                   </div>");


!(function($){
$('#check_generate').click(function() {
    if($('#check_terminate').is(':checked')||$('#check_generate').is(':checked')){
        $('#check_terminate').prop('checked', false);
        $('#input_generate').prop('disabled', false);
        $('#bn_generate').prop('disabled', false);
        $('#bn_generate').removeClass('disabled');

        $('#input_terminate').prop('disabled', true);
        $('#input_terminate').val("");
        $('#bn_terminate').prop('disabled', true);
        $('#bn_terminate').addClass('disabled');
    }
    });
$('#check_terminate').click(function() {
   if($('#check_generate').is(':checked')||$('#check_terminate').is(':checked')){
        $('#check_generate').prop('checked', false);
        $('#input_terminate').prop('disabled', false);
        $('#bn_terminate').prop('disabled', false);
        $('#bn_terminate').removeClass('disabled');

        $('#input_generate').prop('disabled', true);
        $('#input_generate').val("");
        $('#bn_generate').prop('disabled', true);
        $('#bn_generate').addClass('disabled');
   }
   });

   $('#checkbyserial , #checkbyappid').click(function() {
            $('#input_check').prop('disabled', false);
            $('#bn_check').prop('disabled', false);
            $('#bn_check').removeClass('disabled');
            $('#input_check').val("");
         });

   $('#current-time-input').change(function() {
            var min = $('#current-time-input').val();
            $('#selected-time-input').prop('min',min);
            $('#selected-time-input').val("");
          });
})(jQuery);