# Social_media_integration

Social_media_integration is a web application and use Facebook as a social media identity provider
( Login with Facebook )

## Requirement
- [Mariadb](https://downloads.mariadb.org/mariadb/10.5.9/)
- [JDK 1.8](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html)


## Usage

Social_media_integration-0.0.1.jar
``` cmd
java.exe -jar Social_media_integration-0.0.1.jar
```

You can make such jar file as an Docker file by the following methods.
```docker
FROM openjdk:8
RUN mkdir -p /home/SMI
COPY ./Social_media_integration-0.0.1.jar /home/SMI
COPY ./test.socialmediaintegration.com.jks /home/SMI
WORKDIR /home/SMI
EXPOSE 80 443
ENTRYPOINT ["java", "-jar", "Social_media_integration-0.0.1.jar"]
```